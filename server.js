var express = require("express");
var fs = require("fs");
var request = require("request-promise");
var cheerio = require("cheerio");
var app = express();
var path = require("path");
var bodyParser = require("body-parser");
var cors = require("cors");
var scraper = require("./scrape");

app.use(express.static(path.join(__dirname, "public")));
app.use(cors());
app.use(bodyParser.json()); // support json encoded bodies
app.use(
  bodyParser.urlencoded({
    extended: true
  })
); // support encoded bodies

app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname + "/public/index.html"));
});

app.post("/scrapeMobil123", function(req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,Content-Type, Accept"
  ); // If needed

  var searchQuery = "";
  searchQuery = req.body.q;
  searchQuery = searchQuery.split(" ").join("+");
  var list = [];
  scraper
    .getMobil123(searchQuery, {
      condition: req.body.filterCondition,
      location: req.body.filterLocation,
      brand: req.body.filterBrand,
      yearStart: req.body.filterYearStart,
      yearEnd: req.body.filterYearEnd
    })
    .then(result => {
      res.setHeader("Content-Type", "application/json");
      res.send(JSON.stringify(result));
    })
    .catch(err => {
      res.setHeader("Content-Type", "application/json");
      res.send(JSON.stringify(err));
    });
});

app.post("/scrapeOLX", function(req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,Content-Type, Accept"
  ); // If needed

  var searchQuery = "";
  searchQuery = req.body.q;
  searchQuery = searchQuery.split(" ").join("+");
  var list = [];
  scraper
    .getOLX(searchQuery, {
      condition: req.body.filterCondition,
      location: req.body.filterLocation,
      brand: req.body.filterBrand,
      yearStart: req.body.filterYearStart,
      yearEnd: req.body.filterYearEnd
    })
    .then(result => {
      res.setHeader("Content-Type", "application/json");
      res.send(JSON.stringify(result));
    })
    .catch(err => {
      res.setHeader("Content-Type", "application/json");
      res.send(JSON.stringify(err));
    });
});

app.post("/scrapeCarmudi", function(req, res) {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,Content-Type, Accept"
  ); // If needed

  var searchQuery = "";
  searchQuery = req.body.q;
  searchQuery = searchQuery.split(" ").join("+");
  var list = [];
  scraper
    .getCarmudi(searchQuery, {
      condition: req.body.filterCondition,
      location: req.body.filterLocation,
      brand: req.body.filterBrand,
      yearStart: req.body.filterYearStart,
      yearEnd: req.body.filterYearEnd
    })
    .then(result => {
      res.setHeader("Content-Type", "application/json");
      res.send(JSON.stringify(result));
    })
    .catch(err => {
      res.setHeader("Content-Type", "application/json");
      res.send(JSON.stringify(err));
    });
});

app.listen("8084");
console.log("Magic happens on port 8084");

exports = module.exports = app;
