var cheerio = require("cheerio");
var request = require("request-promise");
var puppeteer = require("puppeteer");

module.exports = {
  getMobil123(strQ, filter) {
    return new Promise(function (resolve, reject) {
      let strCondition = "mobil-dijual";
      let strLocation = "indonesia";
      let strBrand = "";
      let strYear = "";

      if (filter.condition != "") {
        if (filter.condition == "Baru") {
          strCondition = "mobil-baru-dijual";
        } else if (filter.condition == "Bekas") {
          strCondition = "mobil-bekas-dijual";
        }
      }

      if (filter.location != "") {
        strLocation =
          strLocation + "_" + filter.location.replace(" ", "-").toLowerCase();
      }

      if (filter.brand != "") {
        strBrand = filter.brand.replace(" ", "-").toLowerCase() + "/";
      }

      if (filter.yearStart != "") {
        strYear = strYear + "&min_year=" + filter.yearStart;
      }

      if (filter.yearEnd != "") {
        strYear = strYear + "&max_year=" + filter.yearEnd;
      }

      let url = `https://www.mobil123.com/${strCondition}/${strBrand}${strLocation}?keyword=${strQ}${strYear}`;
      console.log(url);
      var options = {
        url: url,
        headers: {
          "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0"
        }
      };

      let list = [];
      request(options, function (err, response, html) {
        if (!err) {
          let $ = cheerio.load(html);
          $("article.listing").each(function (k, v) {
            let data = $(this);
            let adName = data.data("title");
            let price = data
              .children()
              .children()
              .last()
              .children()
              .eq(3)
              .children()
              .first()
              .children()
              .text();
            price = price.replace("Rp ", "");
            price = price.split(".").join("");

            let location = data
              .children()
              .children()
              .children()
              .eq(1)
              .children()
              .eq(1)
              .children()
              .eq(2)
              .text();

            let regexYear = /\b(19|20)\d{2}\b/g;
            let year = adName.match(regexYear);
            let strYear = "";
            if (year) {
              strYear = year[0];
            }
            list.push({
              adName,
              price: parseInt(price),
              adUrl: data.data("url"),
              brand: strBrand == "" ? getBrandByTitle(adName) : "",
              imageUrl: data.data("image-src"),
              location,
              siteName: "mobil123",
              year: strYear
            });
          });

          resolve(list);
        } else {
          reject(err);
        }
      });
    });
  },

  getOLX: function (strQ, filter) {
    return new Promise(function (resolve, reject) {
      let strLocation = "";
      let strBrand = "";
      let strYear = "";
      let strOrder = "search[order]=filter_float_price:asc";

      if (filter.location != "") {
        if (
          filter.location.toLowerCase() == "dki jakarta" ||
          filter.location.toLowerCase() == "jabodetabek"
        ) {
          strLocation = "jakarta-dki";
        } else {
          strLocation = filter.location.replace(" ", "-").toLowerCase();
        }
        strLocation = "/" + strLocation;
      }

      if (filter.brand != "") {
        strBrand = "/" + filter.brand.replace(" ", "-").toLowerCase();
      }

      if (filter.yearStart != "") {
        strYear = "search[filter_enum_m_year][0]=" + filter.yearStart;

        if (filter.yearEnd != "") {
          strYear = "";
          new Array(parseInt(filter.yearEnd) - filter.yearStart + 1)
            .fill()
            .map((d, i) => i + filter.yearStart)
            .map((x, y) => {
              strYear =
                strYear + "search[filter_enum_m_year][" + y + "]=" + x + "&";
            });
        }
      }

      let url = `https://www.olx.co.id/mobil/bekas${strBrand}${strLocation}/q-${strQ}/?${strYear}${strOrder}`;
      console.log(url);

      var options = {
        url: url,
        headers: {
          "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0"
        }
      };

      let list = [];
      request(options, function (err, response, html) {
        if (!err) {
          var $ = cheerio.load(html);
          $("table#offers_table tbody tr").each(function (k, v) {
            if (k == 0) {
              return true;
            }
            var data = $(this)
              .children()
              .children()
              .children()
              .children();
            var price = data
              .children()
              .last()
              .children()
              .children()
              .children()
              .text();
            var imageUrl = data
              .children()
              .eq(1)
              .children()
              .children()
              .children()
              .attr("src");
            var adUrl = data
              .children()
              .eq(2)
              .children()
              .children()
              .attr("href");
            var adName = data
              .children()
              .eq(2)
              .children()
              .children()
              .children()
              .text()
              .trim();
            var location = data
              .children()
              .eq(2)
              .children()
              .eq(1)
              .children()
              .children()
              .text();
            var year = data
              .children()
              .eq(2)
              .children()
              .eq(2)
              .children()
              .eq(1)
              .text();

            price = price.replace("Rp ", "");
            price = price.split(".").join("");

            if (price && imageUrl && adUrl && adName) {
              list.push({
                adName,
                price: parseInt(price),
                adUrl,
                brand: strBrand == "" ? getBrandByTitle(adName) : "",
                imageUrl,
                location,
                year,
                siteName: "olx"
              });
            }
          });
          if (!err) {
            resolve(list);
          } else {
            reject(err);
          }
        } else {
          reject(err);
        }
      });
    });
  },

  getCarmudi(strQ, filter) {
    return new Promise(function (resolve, reject) {
      let strCondition = "";
      let strLocation = "indonesia";
      let strBrand = "all";
      let strYear = "";

      if (filter.condition != "") {
        if (filter.condition == "Baru") {
          strCondition = "/new-3";
        } else if (filter.condition == "Bekas") {
          strCondition = "/used";
        }
      }

      if (filter.location != "") {
        strLocation =
          "location:" +
          filter.location.replace(" ", "-").toLowerCase() +
          "/distance:30km/";
      }

      if (filter.brand != "") {
        strBrand = filter.brand.replace(" ", "-").toLowerCase() + "/";
      }

      if (filter.yearStart != "" && filter.yearEnd != "") {
        strYear =
          "year_built:" +
          filter.yearStart.toString() +
          "-" +
          filter.yearEnd.toString() +
          "/";
      }

      //let url = `https://www.mobil123.com/${strCondition}/${strBrand}${strLocation}?keyword=${strQ}${strYear}`;
      let url = `https://www.carmudi.co.id/${strBrand}${strCondition}/q:${strQ}/${strYear}${strLocation}?sort=price-low`;
      console.log(url);
      var options = {
        url: url,
        headers: {
          "User-Agent": "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0"
        }
      };

      (async () => {
        try {
          console.log("ASYNC");
          let list = [];
          const browser = await puppeteer.launch();
          const page = await browser.newPage();
          await page.goto(url, {
            waitUntil: "networkidle2"
          });
          await page.setViewport({
            height: 1600,
            width: 3280
          });
          await page.waitFor(10000);
          await autoScroll(page);
          //await page.close();
          //await browser.close();
          const html = await page.content();
          await page.close();
          let $ = cheerio.load(html);
          $("div.catalog-listing-item").each(function (k, v) {
            let data = $(this);
            let img = data
              .children()
              .eq(1)
              .children()
              .eq(0)
              .children()
              .children()
              .eq(0);
            let button = data
              .find("div.mobile-catalog-listing-item-buttons")
              .children()
              .eq(0);
            let imageUrl = img
              .children()
              .children()
              .eq(0)
              .attr("src");
            let adUrl =
              "https://carmudi.co.id" +
              img
              .children()
              .eq(0)
              .attr("href");
            let adName = button.data("productTitle");
            let price = parseFloat(button.data("productPrice"));
            let brand = button.data("productBrand");
            let location = data
              .find("li.catalog-listing-item-location")
              .children()
              .eq(1)
              .text();

            let regexYear = /\b(19|20)\d{2}\b/g;
            let year = adName.match(regexYear);
            let strYear = "";
            if (year) {
              strYear = year[0];
            }
            if (validateKeywordWithTitle(strQ, adName)) {
              list.push({
                adName,
                price: parseInt(price),
                adUrl,
                brand,
                imageUrl,
                location,
                siteName: "carmudi",
                year: strYear
              });
            }
          });

          resolve(list);
        } catch (ex) {
          reject(ex);
        }
      })();

      // puppeteer
      //   .launch()
      //   .then(function(browser) {
      //     return browser.newPage();
      //   })
      //   .then(function(page) {
      //     page.setViewport({
      //       width: 1200,
      //       height: 800
      //     });
      //     return page.goto(url).then(function() {
      //       return page.content();
      //     });
      //   })
      //   .then(function(html) {
      //     let $ = cheerio.load(html);
      //     $("div.catalog-listing-item").each(function(k, v) {
      //       let data = $(this);
      //       let img = data
      //         .children()
      //         .eq(1)
      //         .children()
      //         .eq(0)
      //         .children()
      //         .children();
      //       let button = data
      //         .find("div.mobile-catalog-listing-item-buttons")
      //         .children()
      //         .eq(0);
      //       let imageUrl = img
      //         .children()
      //         .children()
      //         .eq(0)
      //         .attr("src");
      //       let adUrl =
      //         "https://carmudi.co.id" +
      //         img
      //           .children()
      //           .eq(0)
      //           .attr("href");
      //       let adName = button.data("productTitle");
      //       let price = parseFloat(button.data("productPrice"));
      //       let brand = button.data("productBrand");
      //       let location = data
      //         .find("li.catalog-listing-item-location")
      //         .children()
      //         .eq(1)
      //         .text();

      //       let regexYear = /\b(19|20)\d{2}\b/g;
      //       let year = adName.match(regexYear);
      //       let strYear = "";
      //       if (year) {
      //         strYear = year[0];
      //       }
      //       list.push({
      //         adName,
      //         price: parseInt(price),
      //         adUrl,
      //         brand,
      //         imageUrl,
      //         location,
      //         siteName: "carmudi",
      //         year: strYear
      //       });
      //     });

      //     resolve(list);
      //   })
      //   .catch(function(err) {
      //     reject(err);
      //   });
    });
  }
};

async function autoScroll(page) {
  await page.evaluate(async () => {
    await new Promise((resolve, reject) => {
      var totalHeight = 0;
      var distance = 100;
      var timer = setInterval(() => {
        var scrollHeight = document.body.scrollHeight;
        window.scrollBy(0, distance);
        totalHeight += distance;

        if (totalHeight >= scrollHeight) {
          clearInterval(timer);
          resolve();
        }
      }, 100);
    });
  });
}

let listBrand = [
  "Toyota",
  "Honda",
  "Suzuki",
  "Mitsubishi",
  "Daihatsu",
  "Nissan",
  "Mazda",
  "Mercedes-Benz",
  "Hyundai",
  "Chevrolet"
];

function getBrandByTitle(str) {
  let index = listBrand.findIndex(
    x => str.toLowerCase().indexOf(x.toLowerCase()) >= 0
  );

  if (index >= 0) {
    return listBrand[index];
  } else {
    return "";
  }
}

function validateKeywordWithTitle(keyword, title) {
  keyword.split(' ').forEach((x) => {
    if (title.search(x) < 0) {
      return false;
    }
  });

  return true;
}